package example;

import java.io.IOException;
import java.util.*;
import java.util.regex.*;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class WordCount
{
	public static class Map extends
			Mapper<LongWritable, Text, Text, IntWritable>
	{
		private final static IntWritable one = new IntWritable(1);
		private Text word = new Text();

		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException
		{
			String line = value.toString();
			String regdel = "@.{3}@";
			String http = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#"
				+ "%?=~_|!:,.;]*[-a-zA-Z0-9+&@#%=~_|]/";
			Pattern p1 = Pattern.compile(regdel);
			Matcher m =p1.matcher(line);
			String twopart = new String;
			Pattern p2 = Pattern.compile(http);
			
			int i=0;
			while (m.find())
			{
				if(i%2==0){
					twopart = m.group().toString();
				}
				else{
					Matcher m1 =p2.matcher(m.group().toString());
					if(m1.find()){
						word.set(twopart+m1.group().toString());
						context.write(word, one);
					}
				}
				i++;
			}
		}
	}

	public static class Reduce extends
			Reducer<Text, IntWritable, Text, IntWritable>
	{
		public void reduce(Text key, Iterable<IntWritable> values,
				Context context) throws IOException, InterruptedException
		{
			int sum = 0;
			for (IntWritable val : values)
			{
				sum += val.get();
			}
			context.write(key, new IntWritable(sum));
		}
	}

	public static void main(String[] args) throws Exception
	{
		Configuration conf = new Configuration();

		Job job = new Job(conf, "wordcount");

		job.setJarByClass(WordCount.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);

		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		job.waitForCompletion(true);
	}
}